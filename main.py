import argparse
from logging import getLogger

from anonymization import CsvDiffAnonymizer
from diff import CsvComparator, CsvDiffResult
from diff.printer import CsvDiffPrinter
from report_data import REPORTS_CONFIG, ReportType

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

log = getLogger(__name__)


class ReportsAreDifferentError(BaseException):
    """
    Designates that legacy report doesn't match with new one. After raising task will be failed without
    retrying.
    """


def read_file(file_path):
    try:
        with open(file_path, 'r') as file:
            file_contents = file.read()
            return file_contents
    except FileNotFoundError:
        print(f"File not found: {file_path}")
    except Exception as e:
        print(f"An error occurred: {str(e)}")


def handle_different_reports(
        diff_result: CsvDiffResult,
        report_config: REPORTS_CONFIG,
        max_lines_to_print: int = 0,
        ignore_fields: set = None,
) -> None:
    """
    Handle differences between legacy and BIPS reports
     Raises:
        ReportsAreDifferentError: Raised when reports have differences.
    """
    if not diff_result.has_difference():
        print('Reports are identical!')
        return

    diff_anonymizer = CsvDiffAnonymizer(report_config.anonymization_rules)
    diff_anonymizer.apply(diff_result)
    diff_printer_config = CsvDiffPrinter.Config(
        max_lines_to_print=max_lines_to_print, ignore_fields=ignore_fields
    )
    diff_printer = CsvDiffPrinter(diff_printer_config)
    diff_printer.print(diff_result)
    raise ReportsAreDifferentError()


# start


parser = argparse.ArgumentParser(description='Process some parameters.')
parser.add_argument('--legacy_report', type=str, help='path for legacy report file')
parser.add_argument('--bips_report', type=str, help='path for bips report file')
parser.add_argument('--ignore_fields', type=str, help='path for bips report file', default='')
parser.add_argument('--lines_to_print', type=int, help='path for bips report file', default=30000)

parser.add_argument(
    '--type', type=str,
    help='report type: transaction, deposit, transaction_pending, customer, cashflow, position'
)

args = parser.parse_args()

legacy_report_path = args.legacy_report
bips_report_path = args.bips_report
report_type = ReportType(args.type)
ignore_fields = args.ignore_fields
lines_to_print = args.lines_to_print

print(
    f"Params: legacy_report_path={legacy_report_path}, "
    f"bips_report_path={bips_report_path}, "
    f"report_type={report_type}, "
    f"ignore_fields={ignore_fields}, "
    f"lines_to_print={lines_to_print}"
)

report_config = REPORTS_CONFIG[report_type]
comparator = CsvComparator(strategy=report_config.comparison_strategy)

legacy_report = read_file(legacy_report_path)
bips_report = read_file(bips_report_path)

diff_result = comparator.compare(legacy_report, bips_report)

ignore_fields_set = set(ignore_fields.split(','))

handle_different_reports(diff_result, report_config, lines_to_print, ignore_fields)
