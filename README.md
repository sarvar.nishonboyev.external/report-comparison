# Report Comparison script

## Steps 

- download report file to local machine, decrypt the file if encrypted (only for ASE)
- run the script by showing absolute path to the report files
- logs are printed in a file under `./logs/` directory 

**How to run the report comparison script?**

```
 python3 main.py --legacy_report=/path/hosting_transaction_3_5_insta.csv \
 --bips_report=/path/hosting_transaction_5_5_insta.csv \
 --type=transaction \
 --ingore_fields='tax-source' \
 --lines_to_print=30000
 
```

### Parameters

report types

- transaction
- deposit
- transaction_pending
- customer
- cashflow
- position