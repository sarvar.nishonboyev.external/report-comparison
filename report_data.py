from dataclasses import dataclass, field
from enum import Enum
from typing import Dict, List

from anonymization import Mask, Masks
from diff.strategies import (
    CsvComparisonStrategy,
    OneToOneComparisonStrategy,
    RowIdAwareComparisonStrategy,
)


class ReportType(Enum):
    TRANSACTION = 'transaction'
    DEPOSIT = 'deposit'
    TRANSACTION_PENDING = 'transaction_pending'
    CUSTOMER = 'customer'
    CASHFLOW = 'cashflow'
    POSITION = 'position'


@dataclass
class ReportsLocation:
    legacy_report: str
    bips_report: str


@dataclass
class ReportComparisonConfig:
    comparison_strategy: CsvComparisonStrategy = field(default_factory=OneToOneComparisonStrategy)
    anonymization_rules: Dict[str, List[Mask]] = field(default_factory=dict)


hosting_transaction_report_config = ReportComparisonConfig(
    comparison_strategy=RowIdAwareComparisonStrategy(['transaction-id']),
    anonymization_rules={
        'transaction-account-iban': [Masks.iban()],
        'target-account-IBAN': [Masks.iban()],
        'source-account-IBAN': [Masks.iban()],
    },
)

hosting_deposit_report_config = ReportComparisonConfig(
    comparison_strategy=RowIdAwareComparisonStrategy(['term-deposit-id']),
    anonymization_rules={
        'kyc-questions': [Masks.hardcode()],
        'transaction-account-iban': [Masks.iban()],
    },
)

customer_report_config = ReportComparisonConfig(
    comparison_strategy=RowIdAwareComparisonStrategy(['customer-id']),
    anonymization_rules={
        'address-additional-info': [Masks.hardcode()],
        'address-country': [Masks.hardcode()],
        'address-postal-cd': [Masks.hardcode()],
        'address-street': [Masks.hardcode()],
        'address-town': [Masks.hardcode()],
        'birth-dt': [Masks.hardcode()],
        'birth-name': [Masks.hardcode()],
        'email-address': [Masks.hardcode()],
        'first-names': [Masks.hardcode()],
        'id-expiry-dt': [Masks.hardcode()],
        'id-issue-dt': [Masks.hardcode()],
        'id-number': [Masks.hardcode()],
        'last-name': [Masks.hardcode()],
        'phone-number': [Masks.hardcode()],
        'place-of-birth': [Masks.hardcode()],
        'reference-account-iban': [Masks.hardcode()],
        'tax-id': [Masks.hardcode()],
        'transaction-account-iban': [Masks.iban()],
    },
)

cashflow_report_config = ReportComparisonConfig(
    comparison_strategy=RowIdAwareComparisonStrategy(['aggregate-transaction-id']),
    anonymization_rules={
        'target-account-IBAN': [Masks.iban()],
        'source-account-IBAN': [Masks.iban()],
    },
)

position_report_config = ReportComparisonConfig(
    comparison_strategy=RowIdAwareComparisonStrategy(['aggregate-position-id']),
    anonymization_rules={},
)

REPORTS_CONFIG: Dict[ReportType, ReportComparisonConfig] = {
    ReportType.TRANSACTION: hosting_transaction_report_config,
    ReportType.DEPOSIT: hosting_deposit_report_config,
    ReportType.CUSTOMER: customer_report_config,
    ReportType.CASHFLOW: cashflow_report_config,
    ReportType.POSITION: position_report_config,
}
