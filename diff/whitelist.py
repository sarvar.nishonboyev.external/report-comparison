from dataclasses import dataclass, field
from enum import Enum
from logging import getLogger
from typing import Dict, List, Optional

from reports.reports_comparison_dag.diff.data import ColumnDiff, CsvDiffResult, DiffType, Row, RowDiff
from utils import list_safe_get

log = getLogger(__name__)


class DiffWhitelistChangeMode(int, Enum):
    ANY_VALUE = 0
    MATCH_OLD_VALUE = 1
    MATCH_NEW_VALUE = 2
    MATCH_BOTH_VALUES = 3


@dataclass
class DiffWhitelistChangeEntry:
    column: str
    mode: DiffWhitelistChangeMode = DiffWhitelistChangeMode.ANY_VALUE
    old_value: Optional[str] = None
    new_value: Optional[str] = None


@dataclass
class DiffWhitelistEntry:
    record_filter: Dict[str, str]
    diff_type_filter: Optional[DiffType] = None
    changes_filter: List[DiffWhitelistChangeEntry] = field(default_factory=list)


class DiffWhitelist:
    def __init__(self, entries: List[DiffWhitelistEntry] = None):
        self.__entries = entries or []

    def apply(self, csv_diff: CsvDiffResult) -> None:
        columns_ids = self.__get_columns_ids(csv_diff)

        self.__validate_whitelist_entries(columns_ids)

        rows_to_whitelist = []
        for i, row_diff in enumerate(csv_diff.diff):
            for entry in self.__entries:
                if not self.__is_row_diff_whitelisted_by_entry(row_diff, entry, columns_ids):
                    continue
                if (
                    not entry.diff_type_filter
                    or entry.diff_type_filter != DiffType.CHANGED
                    or (entry.diff_type_filter == DiffType.CHANGED and not entry.changes_filter)
                ):
                    rows_to_whitelist.append(i)
                    break

                self.__apply_changes_whitelisting(row_diff, entry, columns_ids)
                if not row_diff.columns_diff:
                    rows_to_whitelist.append(i)
                    break

        for i in reversed(rows_to_whitelist):
            del csv_diff.diff[i]

        csv_diff.update_statistic()

    @staticmethod
    def __apply_changes_whitelisting(
        row_diff: RowDiff, entry: DiffWhitelistEntry, columns_ids: Dict[str, int]
    ) -> None:
        changes_to_whitelist = []
        for i, col_diff in enumerate(row_diff.columns_diff):
            for change_filter in entry.changes_filter:
                if columns_ids[change_filter.column] != col_diff.column_number:
                    continue
                if DiffWhitelist.__is_change_whitelisted_by_entry(col_diff, change_filter):
                    changes_to_whitelist.append(i)
                    break

        for i in reversed(changes_to_whitelist):
            del row_diff.columns_diff[i]

    @staticmethod
    def __is_change_whitelisted_by_entry(
        col_diff: ColumnDiff, change_filter: DiffWhitelistChangeEntry
    ) -> bool:
        any_change_match = change_filter.mode == DiffWhitelistChangeMode.ANY_VALUE
        new_value_match = (
            change_filter.mode == DiffWhitelistChangeMode.MATCH_NEW_VALUE
            and col_diff.new_value == change_filter.new_value
        )
        old_value_match = (
            change_filter.mode == DiffWhitelistChangeMode.MATCH_OLD_VALUE
            and col_diff.old_value == change_filter.old_value
        )
        both_values_match = (
            change_filter.mode == DiffWhitelistChangeMode.MATCH_BOTH_VALUES
            and col_diff.old_value == change_filter.old_value
            and col_diff.new_value == change_filter.new_value
        )
        return any_change_match or new_value_match or old_value_match or both_values_match

    def __validate_whitelist_entries(self, columns_ids: Dict[str, int]) -> None:
        self.__entries = [
            entry for entry in self.__entries if self.__is_whitelist_entry_valid(entry, columns_ids)
        ]

    @staticmethod
    def __is_whitelist_entry_valid(entry: DiffWhitelistEntry, columns_ids: Dict[str, int]) -> bool:
        for filter_header in entry.record_filter.keys():
            if filter_header not in columns_ids:
                log.warning(
                    'The whitelist entry is not valid. Column "%s" does not exist in the report',
                    filter_header,
                )
                return False
        for change_filter in entry.changes_filter:
            if change_filter.column not in columns_ids:
                log.warning(
                    'The whitelist entry is not valid. Column "%s" does not exist in the report',
                    change_filter.column,
                )
                return False
            if change_filter.mode == DiffWhitelistChangeMode.MATCH_NEW_VALUE and not change_filter.new_value:
                log.warning(
                    'The whitelist entry is not valid.'
                    ' Change filter for column "%s" set to mode "MATCH_NEW_VALUE",'
                    ' but doesn\'t have "new_value"',
                    change_filter.column,
                )
                return False
            if change_filter.mode == DiffWhitelistChangeMode.MATCH_OLD_VALUE and not change_filter.old_value:
                log.warning(
                    'The whitelist entry is not valid.'
                    ' Change filter for column "%s" set to mode "MATCH_OLD_VALUE",'
                    ' but doesn\'t have "old_value"',
                    change_filter.column,
                )
                return False
            if (
                change_filter.mode == DiffWhitelistChangeMode.MATCH_BOTH_VALUES
                and not change_filter.new_value
                and not change_filter.old_value
            ):
                log.warning(
                    'The whitelist entry is not valid.'
                    ' Change filter for column "%s" set to mode "MATCH_BOTH_VALUES",'
                    ' but doesn\'t have "new_value" or "old_value"',
                    change_filter.column,
                )
                return False

        return True

    def __is_row_diff_whitelisted_by_entry(
        self, row_diff: RowDiff, entry: DiffWhitelistEntry, columns_ids: Dict[str, int]
    ) -> bool:
        if not self.__is_row_diff_matching_record_filter(row_diff, entry.record_filter, columns_ids):
            return False
        if not entry.diff_type_filter or entry.diff_type_filter == row_diff.diff_type:
            return True
        return False

    @staticmethod
    def __is_row_diff_matching_record_filter(
        row_diff: RowDiff, record_filter: Dict[str, str], columns_ids: Dict[str, int]
    ) -> bool:
        for column, value in record_filter.items():
            if row_diff.row[columns_ids[column]] != value:
                return False
        return True

    @staticmethod
    def __get_columns_ids(csv_diff: CsvDiffResult) -> Dict[str, int]:
        header_row = list_safe_get(csv_diff.source_csv, 0) or list_safe_get(csv_diff.target_csv, 0, Row())
        return {column_name: column_id for (column_id, column_name) in enumerate(header_row.values)}
