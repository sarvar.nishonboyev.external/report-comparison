import datetime
import os
from dataclasses import dataclass
from io import StringIO
from logging import getLogger
from typing import Dict, List, Optional, Set, TextIO

from diff import CsvDiffResult
from diff.data import DiffType, RowDiff, CsvDiffStatistic, Row
from utils import chunks, list_safe_get

log = getLogger(__name__)


class CsvDiffPrinter:
    @dataclass
    class Config:
        print_statistic: bool = True
        print_difference: bool = True

        delimiter: str = ','

        moved_row_char: str = '⇌'
        added_row_char: str = '+'
        removed_row_char: str = '-'
        changed_row_char: str = '~'

        use_quotes: bool = True
        quote_char: str = '\''

        align_columns: bool = True

        use_diff_highlighting: bool = True
        diff_highlighting_char: str = '~'

        max_lines_to_print: int = 0
        ignore_fields: Optional[Set] = None

    def __init__(self, config: Config = None):
        self.__config = config or CsvDiffPrinter.Config()

    def print(self, csv_diff: CsvDiffResult) -> None:
        current_time = datetime.datetime.now()
        timestamp_str = current_time.strftime("%Y%m%d%H%M%S")

        log_directory = "logs"

        if not os.path.exists(log_directory):
            os.makedirs(log_directory)

        output = open(os.path.join(log_directory, f"result_{timestamp_str}.log"), "a")
        # f = open("result.log", "a")

        output.write('Reports difference:\n')

        if self.__config.print_statistic:
            self.__print_statistic(csv_diff.statistic, output)
            output.write('\n')
        if self.__config.print_difference:
            self.__print_difference(csv_diff, output)

        # diff_to_print = output.getvalue()
        # lines_to_print = diff_to_print.splitlines()

        # max_lines = len(lines_to_print)
        # if self.__config.max_lines_to_print > 0:
        #     max_lines = self.__config.max_lines_to_print
        #     log.info("%s line of logs will be printed", self.__config.max_lines_to_print)

        # lines_to_print = lines_to_print[0:max_lines]

        # Workaround to bypass log truncation in Airflow. The Airflow warning message:
        # 'Log message size exceeds CWL max payload size, truncated'
        # for diff_chunk in chunks(lines_to_print, 200):
        #     log.info('\n' + '\n'.join(diff_chunk))

    def __print_difference(self, csv_diff: CsvDiffResult, output: TextIO) -> None:
        config = self.__config
        header_row = list_safe_get(csv_diff.source_csv, 0) or list_safe_get(csv_diff.target_csv, 0, Row())
        max_columns_width = (
            self.__find_max_columns_width(csv_diff, header_row.values) if config.align_columns else {}
        )
        column_names = header_row.values

        ignore_fields_set = self.__config.ignore_fields or set()

        log.info('ignore_fields_set=%s', ignore_fields_set)

        header_row_formatted = self.__config.delimiter.join(
            self.__format_row(header_row.values, max_columns_width)
        )
        output.write(f'  {header_row_formatted}\n')

        for row_diff in csv_diff.diff:
            if row_diff.diff_type == DiffType.ADDED:
                self.__print_added_row(output, row_diff, max_columns_width)
            elif row_diff.diff_type == DiffType.REMOVED:
                self.__print_removed_row(output, row_diff, max_columns_width)
            elif row_diff.diff_type == DiffType.CHANGED:
                # Check if any of the changed columns are in the ignore list, if so, don't print it
                if not all(
                    column_names[cd.column_number] in ignore_fields_set for cd in row_diff.columns_diff
                ):
                    self.__print_changed_row(output, row_diff, max_columns_width)
            elif row_diff.diff_type == DiffType.MOVED:
                self.__print_moved_row(output, row_diff, max_columns_width)

    def __print_moved_row(
        self, output: StringIO, row_diff: RowDiff, max_columns_width: Dict[int, int]
    ) -> None:
        row_string = self.__config.delimiter.join(self.__format_row(row_diff.row, max_columns_width))
        self.__print_line_number(output, self.__config.moved_row_char, row_diff)
        output.write(f'  {row_string}\n')

    def __print_added_row(
        self, output: StringIO, row_diff: RowDiff, max_columns_width: Dict[int, int]
    ) -> None:
        row_string = self.__config.delimiter.join(self.__format_row(row_diff.row, max_columns_width))
        self.__print_line_number(output, self.__config.added_row_char, row_diff)
        output.write(f'  {row_string}\n')

    def __print_removed_row(
        self, output: StringIO, row_diff: RowDiff, max_columns_width: Dict[int, int]
    ) -> None:
        row_string = self.__config.delimiter.join(self.__format_row(row_diff.row, max_columns_width))
        self.__print_line_number(output, self.__config.removed_row_char, row_diff)
        output.write(f'  {row_string}\n')

    def __print_changed_row(
        self, output: StringIO, row_diff: RowDiff, max_columns_width: Dict[int, int]
    ) -> None:
        source_row = row_diff.row
        target_row = source_row.copy()
        for column_diff in row_diff.columns_diff:
            target_row[column_diff.column_number] = column_diff.new_value

        source_row_string = self.__config.delimiter.join(self.__format_row(source_row, max_columns_width))
        target_row_formatted = self.__format_row(target_row, max_columns_width)
        target_row_string = self.__config.delimiter.join(target_row_formatted)
        self.__print_line_number(output, self.__config.changed_row_char, row_diff)
        output.write(f'  {source_row_string}\n  {target_row_string}\n')

        if self.__config.use_diff_highlighting:
            pad_correction = self.__get_pad_correction()
            highlight_char = self.__config.diff_highlighting_char
            highlighting_row = list(
                map(lambda value: ' ' * (len(value) - pad_correction), target_row_formatted)
            )
            for column_diff in row_diff.columns_diff:
                column_width_filler = list_safe_get(highlighting_row, column_diff.column_number, '')
                highlighting_row[column_diff.column_number] = highlight_char * len(column_width_filler)

            row_left_padding = 2
            highlighting_delimiter = ' ' * (len(self.__config.delimiter) + pad_correction)
            highlighting_row_string = highlighting_delimiter.join(highlighting_row)
            output.write((' ' * int(row_left_padding + pad_correction / 2)) + highlighting_row_string + '\n')

    def __format_row(self, row: List[str], max_columns_width: Dict[int, int]) -> List[str]:
        formatted_row: List[str] = []
        for (column_id, value) in enumerate(row):
            if self.__config.use_quotes:
                value = f'{self.__config.quote_char}{value}{self.__config.quote_char}'
            value = value.ljust(max_columns_width.get(column_id, 0) + self.__get_pad_correction())
            formatted_row.append(value)
        return formatted_row

    def __get_pad_correction(self) -> int:
        return len(self.__config.quote_char) * 2 if self.__config.use_quotes else 0

    @staticmethod
    def __find_max_columns_width(csv_diff: CsvDiffResult, header_row: List[str]) -> Dict[int, int]:
        max_columns_width: Dict[int, int] = CsvDiffPrinter.__find_max_header_columns_width(header_row)

        for row_diff in csv_diff.diff:
            for row_column_id in range(len(row_diff.row)):
                column_width = len(row_diff.row[row_column_id])
                if column_width > max_columns_width.get(row_column_id, 0):
                    max_columns_width[row_column_id] = column_width

            for column_diff in row_diff.columns_diff:
                if column_diff.new_value:
                    column_width = len(column_diff.new_value)
                    if column_width > max_columns_width.get(column_diff.column_number, 0):
                        max_columns_width[column_diff.column_number] = column_width

        return max_columns_width

    @staticmethod
    def __find_max_header_columns_width(header_row: List[str]) -> Dict[int, int]:
        max_columns_width: Dict[int, int] = {}
        for (column_id, value) in enumerate(header_row):
            max_columns_width[column_id] = len(value)
        return max_columns_width

    @staticmethod
    def __print_line_number(output: StringIO, action_char: str, row_diff: RowDiff) -> None:
        source_row_number = row_diff.source_row_number if row_diff.source_row_number >= 0 else '-'
        target_row_number = row_diff.target_row_number if row_diff.target_row_number >= 0 else '-'
        output.write(f'{action_char} {source_row_number}:{target_row_number}\n')

    @staticmethod
    def __print_statistic(diff_stats: CsvDiffStatistic, output: TextIO) -> None:
        output.write(
            f"""\
Total rows:     {diff_stats.total_rows}
All changes:    {diff_stats.all_changes}
Rows added:     {diff_stats.rows_added}
Rows removed:   {diff_stats.rows_removed}
Rows changed:   {diff_stats.rows_changed}
Rows moved:     {diff_stats.rows_moved}
Values changed: {diff_stats.values_changed}
"""
        )
