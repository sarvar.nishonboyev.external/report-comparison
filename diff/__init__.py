import csv
from csv import Dialect
from dataclasses import dataclass
from io import StringIO
from logging import getLogger
from typing import List, Optional, Type, Union

from csv_utils import build_dialect
from diff.data import CsvDiffResult, Row
from diff.strategies import CsvComparisonStrategy, OneToOneComparisonStrategy

log = getLogger(__name__)


class CsvParser:
    @dataclass
    class Config:
        dialect: Optional[Union[str, Dialect, Type[Dialect]]] = None

    def __init__(self, config: Config = None):
        self.__config = config or CsvParser.Config()

    def parse(self, raw_csv: str) -> List[Row]:
        dialect = self.__config.dialect or build_dialect()
        reader = csv.reader(StringIO(raw_csv, newline=''), dialect=dialect)

        rows = []
        row_number = 1
        for row in reader:
            rows.append(Row(number=row_number, values=row))
            row_number += 1
        return rows


class CsvComparator:
    def __init__(self, strategy: CsvComparisonStrategy = None, csv_parser: CsvParser = None):
        self.__parser = csv_parser or CsvParser()
        self.__strategy = strategy or OneToOneComparisonStrategy()

    def compare(self, raw_source_csv: str, raw_target_csv: str) -> CsvDiffResult:
        log.info('Parsing CSVs')
        source = self.__parser.parse(raw_source_csv)
        target = self.__parser.parse(raw_target_csv)

        log.info('Comparing CSVs using strategy=%s', type(self.__strategy).__name__)
        diff = self.__strategy.compare(source, target)
        diff_result = CsvDiffResult(diff=diff, source_csv=source, target_csv=target)

        log.info('Updating diff statistics')
        diff_result.update_statistic()

        return diff_result
