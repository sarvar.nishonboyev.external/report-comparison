from dataclasses import dataclass, field
from enum import Enum
from typing import List


@dataclass
class Row:
    number: int = -1
    values: List[str] = field(default_factory=list)


class DiffType(Enum):
    MOVED = 0
    ADDED = 1
    REMOVED = 2
    CHANGED = 3
    UNTOUCHED = 4


@dataclass
class ColumnDiff:
    column_number: int = 0
    old_value: str = ''
    new_value: str = ''


@dataclass
class RowDiff:
    source_row_number: int = -1
    target_row_number: int = -1
    row: List[str] = field(default_factory=list)
    diff_type: DiffType = DiffType.UNTOUCHED
    columns_diff: List[ColumnDiff] = field(default_factory=list)


@dataclass
class CsvDiffStatistic:
    all_changes: int = 0
    total_rows: int = 0
    rows_added: int = 0
    rows_removed: int = 0
    rows_changed: int = 0
    rows_moved: int = 0
    values_changed: int = 0


@dataclass
class CsvDiffResult:
    diff: List[RowDiff]
    source_csv: List[Row]
    target_csv: List[Row]
    statistic: CsvDiffStatistic = field(default_factory=CsvDiffStatistic)

    def update_statistic(self) -> None:
        self.statistic = CsvDiffStatistic()
        for row in self.diff:
            self.statistic.rows_added += 1 if row.diff_type == DiffType.ADDED else 0
            self.statistic.rows_removed += 1 if row.diff_type == DiffType.REMOVED else 0
            self.statistic.rows_changed += 1 if row.diff_type == DiffType.CHANGED else 0
            self.statistic.rows_moved += 1 if row.diff_type == DiffType.MOVED else 0
            self.statistic.values_changed += len(row.columns_diff)
        self.statistic.all_changes += self.statistic.rows_added
        self.statistic.all_changes += self.statistic.rows_removed
        self.statistic.all_changes += self.statistic.values_changed
        self.statistic.total_rows = max(len(self.source_csv), len(self.target_csv))

    def has_difference(self) -> bool:
        return bool(self.diff)
