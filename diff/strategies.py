from abc import abstractmethod
from logging import getLogger
from typing import Callable, Dict, List

from diff import Row
from diff.data import RowDiff, DiffType, ColumnDiff
# from reports.reports_comparison_dag.diff.data import ColumnDiff, DiffType, Row, RowDiff
from utils import list_safe_get, list_safe_index

log = getLogger(__name__)


def compare_rows(source_row: Row, target_row: Row) -> RowDiff:
    if not source_row.values and target_row.values:
        return RowDiff(row=target_row.values, diff_type=DiffType.ADDED)
    if source_row.values and not target_row.values:
        return RowDiff(row=source_row.values, diff_type=DiffType.REMOVED)

    columns_diff = []
    columns_count = max(len(source_row.values), len(target_row.values))
    for column_number in range(columns_count):
        source_column = list_safe_get(source_row.values, column_number, '')
        target_column = list_safe_get(target_row.values, column_number, '')
        if source_column != target_column:
            column_diff = ColumnDiff(column_number, source_column, target_column)
            columns_diff.append(column_diff)

    if not columns_diff:
        if source_row.number != target_row.number:
            return RowDiff(row=source_row.values, diff_type=DiffType.MOVED)
        return RowDiff(row=source_row.values, diff_type=DiffType.UNTOUCHED)

    return RowDiff(row=source_row.values, diff_type=DiffType.CHANGED, columns_diff=columns_diff)


class CsvComparisonStrategy:
    @abstractmethod
    def compare(self, source: List[Row], target: List[Row]) -> List[RowDiff]:
        raise NotImplementedError("compare is abstract!")


class OneToOneComparisonStrategy(CsvComparisonStrategy):
    def compare(self, source: List[Row], target: List[Row]) -> List[RowDiff]:
        diff = []
        rows_count = max(len(source), len(target))
        for row_number in range(rows_count):
            source_row = list_safe_get(source, row_number, Row())
            target_row = list_safe_get(target, row_number, Row())
            row_diff = compare_rows(source_row, target_row)
            if row_diff.diff_type != DiffType.UNTOUCHED:
                row_diff.source_row_number = source_row.number
                row_diff.target_row_number = target_row.number
                diff.append(row_diff)
        return diff


class RowIdAwareComparisonStrategy(CsvComparisonStrategy):
    def __init__(self, row_id_columns: List[str]):
        log.info("row_id_columns: %s", row_id_columns)
        self.__row_id_columns = row_id_columns
        self.__one_to_one_strategy = OneToOneComparisonStrategy()

    def compare(self, source: List[Row], target: List[Row]) -> List[RowDiff]:
        header_row: Row = list_safe_get(source, 0) or list_safe_get(target, 0, Row())
        row_id_column_ids_raw = map(lambda r: list_safe_index(header_row.values, r), self.__row_id_columns)
        row_id_column_ids = list(filter(lambda x: x is not None, row_id_column_ids_raw))

        source_grouped = self.__group_by_row_id(source, row_id_column_ids)
        target_grouped = self.__group_by_row_id(target, row_id_column_ids)

        row_ids = set(source_grouped.keys())
        row_ids.update(target_grouped.keys())

        diff = []
        added_rows_ids = []
        removed_rows_ids = []
        for row_id in row_ids:
            source_rows = source_grouped.get(row_id, [])
            target_rows = target_grouped.get(row_id, [])
            diffs = self.__one_to_one_strategy.compare(source_rows, target_rows)
            for row_diff in diffs:
                if row_diff.diff_type == DiffType.REMOVED:
                    removed_rows_ids.append(row_diff.source_row_number)
                if row_diff.diff_type == DiffType.ADDED:
                    added_rows_ids.append(row_diff.target_row_number)
            diff.extend(diffs)

        diff.sort(key=lambda d: max(d.source_row_number, d.target_row_number))
        added_rows_ids.sort()
        removed_rows_ids.sort()

        diff = list(filter(self.__get_moved_rows_filter(added_rows_ids, removed_rows_ids), diff))

        return diff

    @staticmethod
    def __get_moved_rows_filter(
        added_rows_ids: List[int], removed_rows_ids: List[int]
    ) -> Callable[[RowDiff], bool]:
        moved_rows = []

        def filter_moved_rows(row_diff: RowDiff) -> bool:
            if row_diff.diff_type == DiffType.MOVED:
                source_number = row_diff.source_row_number
                target_number = row_diff.target_row_number

                # Check for duplicates
                if (target_number, source_number) in moved_rows:
                    return False

                source_delta = RowIdAwareComparisonStrategy.__find_delta_for_row_number(
                    source_number, removed_rows_ids
                )
                target_delta = RowIdAwareComparisonStrategy.__find_delta_for_row_number(
                    target_number, added_rows_ids
                )
                row_delta = (source_number - source_delta) - (target_number - target_delta)
                if row_delta == 0:
                    return False
                else:
                    moved_rows.append((source_number, target_number))
            return True

        return filter_moved_rows

    @staticmethod
    def __find_delta_for_row_number(row_number: int, rows_before: List[int]) -> int:
        delta = 0
        for row in rows_before:
            if row < row_number:
                delta += 1
            else:
                break
        return delta

    @staticmethod
    def __group_by_row_id(csv: List[Row], row_id_columns: List[int]) -> Dict[str, List[Row]]:
        result: Dict[str, List[Row]] = {}
        for row in csv:
            row_id = RowIdAwareComparisonStrategy.__get_row_id(row, row_id_columns)
            if row_id not in result:
                result[row_id] = []
            else:
                log.warning('Row ID=%s is not unique!', row_id)
            result[row_id].append(row)

        return result

    @staticmethod
    def __get_row_id(row: Row, row_id_columns: List[int]) -> str:
        return '/'.join([list_safe_get(row.values, column_id, '') for column_id in row_id_columns])
