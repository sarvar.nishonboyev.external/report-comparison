import csv
from typing import Type


def build_dialect(delimiter: str = ',') -> Type[csv.Dialect]:
    """
    Creates and returns standard dialect for reading and writing CSV files, which:
    - Wraps all values double quote character - "
    - Uses two double quote character to escape double quote in the value: "The ""escaped"" value"
    - Uses '\n' character as a line terminator
    :param delimiter: Character that will be used to separate values in CSV
    :return: Dialect object
    """
    dialect = csv.Dialect
    dialect.delimiter = delimiter
    dialect.quotechar = '"'
    dialect.doublequote = True
    dialect.lineterminator = '\n'
    dialect.quoting = csv.QUOTE_ALL
    return dialect
