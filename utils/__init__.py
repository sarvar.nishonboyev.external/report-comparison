from enum import Enum
from typing import Any, Iterator, List, Optional


class AirflowEnv(Enum):
    """
    Airflow environment name set in Airflow variables.
    """

    DEVELOPMENT = 'dev'
    STAGING = 'staging'
    ONBOARDING = 'onboarding'
    PRODUCTION = 'production'


def list_safe_get(target_list: List[Any], index: int, fallback: Any = None) -> Any:
    """
    Returns list element by index suppressing IndexError if index is out of bounds.
    :param target_list: List to get element from.
    :param index: Index of element to get.
    :param fallback: Default value that will be returned if given index is out of list bounds.
    :return: List element at index or fallback
    """
    try:
        return target_list[index]
    except IndexError:
        return fallback


def dict_get_safe(dictionary: Optional[dict], key: Any, fallback: Any = None) -> Any:
    """
    Returns dictionary value if dictionary is not None and the key exists.
    :param dictionary: Dictionary to get value from or None.
    :param key: Key of value to get.
    :param fallback: Default value that will be returned if given key is not present.
    :return: Dictionary value at key or fallback
    """
    return dictionary[key] if dictionary and key in dictionary else fallback


def list_safe_index(target_list: List[Any], value: str, **kwargs: Any) -> Optional[int]:
    """
    Return index of value in the list or None if value isn't there.
    Unlike list index method it doesn't throw ValueError if value is not in the list.
    :param target_list: List to find index from.
    :param value: Value to look.
    :param kwargs: Other arguments that is passed to the index method. I.e. 'start' or 'end' params
    :return: Index of the value in the list or None if value is not present.
    """
    return target_list.index(value, **kwargs) if value in target_list else None


def chunks(target_list: List[Any], chunk_size: int) -> Iterator[List[str]]:
    """
    Splits the given list into chunks.
    :param target_list: The list to split by chunks
    :param chunk_size: The size of one chunk
    :return: Generator that iterates through chunks
    """
    for i in range(0, len(target_list), chunk_size):
        yield target_list[i : i + chunk_size]
