import re
from abc import abstractmethod
from logging import getLogger
from typing import Dict, List, Optional, Union

from diff.data import ColumnDiff, CsvDiffResult
from utils import list_safe_get, list_safe_index

log = getLogger(__name__)

DEFAULT_MASK = '******'


class Mask:
    fallback: str = DEFAULT_MASK

    def apply(self, value: str) -> str:
        try:
            return self._apply_internal(value)
        except Exception:
            log.warning('Exception happened during masking, using fallback.')
            return self.fallback

    @abstractmethod
    def _apply_internal(self, value: str) -> str:
        """
        :return: Anonymized value
        """
        raise NotImplementedError('_apply_internal is abstract')


class RegexpMask(Mask):
    def __init__(self, pattern: str, mask_char: str = '*'):
        self.__pattern = pattern
        self.__mask_char = mask_char

    def _apply_internal(self, value: str) -> str:
        return re.sub(self.__pattern, lambda x: self.__mask_char * len(x.group()), value)


class HardCodeMask(Mask):
    def __init__(self, hard_coded_value: str = DEFAULT_MASK):
        self.__hard_coded_value = hard_coded_value

    def _apply_internal(self, value: str) -> str:
        return self.__hard_coded_value


class KycQuestionsMask(Mask):
    """
    It masks only answers in KYC questions. For example:
    question1==answer_to_question1|question2==answer_to_question2
    will result in:
    question1==******|question2==******
    """

    question_delimiter: str = '|'
    answer_delimiter: str = '=='

    def __init__(self, answer_mask: Mask):
        self.__answer_mask = answer_mask

    def _apply_internal(self, value: str) -> str:
        questions = value.split(self.question_delimiter)
        for question_id in range(len(questions)):
            question = questions[question_id]
            question_answer = question.split(self.answer_delimiter)
            answer = list_safe_get(question_answer, 1)
            if answer:
                question_answer[1] = self.__answer_mask.apply(answer)
            questions[question_id] = self.answer_delimiter.join(question_answer)
        return self.question_delimiter.join(questions)


class Masks:
    @staticmethod
    def iban(mask_char: str = '*') -> Mask:
        return RegexpMask('.{4}$', mask_char)

    @staticmethod
    def hardcode(value: str = DEFAULT_MASK) -> Mask:
        return HardCodeMask(value)

    @staticmethod
    def kyc_questions(answer_mask: Optional[Mask] = None) -> Mask:
        return KycQuestionsMask(answer_mask or Masks.hardcode())


class CsvDiffAnonymizer:
    def __init__(self, rules: Dict[str, List[Mask]] = None):
        self.__rules = rules or {}

    def add_rule(self, column_name: Union[str, List[str]], mask: Mask) -> None:
        column_names = [column_name] if isinstance(column_name, str) else column_name
        for column in column_names:
            if column not in self.__rules:
                self.__rules[column] = []
            self.__rules[column].append(mask)

    def apply(self, diff_result: CsvDiffResult) -> None:
        masks_per_column_id: Dict[int, List[Mask]] = self.__get_masks_per_column_id(diff_result)

        for row_diff in diff_result.diff:
            for column_id, masks in masks_per_column_id.items():
                self.__apply_mask_to_row(row_diff.row, column_id, masks)
                self.__apply_mask_to_columns_diff(row_diff.columns_diff, column_id, masks)

    def __get_masks_per_column_id(self, diff_result: CsvDiffResult) -> Dict[int, List[Mask]]:
        header_row = list_safe_get(diff_result.source_csv, 0) or list_safe_get(diff_result.target_csv, 0)
        if not header_row:
            return {}

        masks_per_column_id: Dict[int, List[Mask]] = {}
        for column_name in self.__rules.keys():
            column_id = list_safe_index(header_row.values, column_name)
            if column_id is not None:
                masks_per_column_id[column_id] = self.__rules[column_name]

        return masks_per_column_id

    def __apply_mask_to_row(self, row: List[str], column_id: int, masks: List[Mask]) -> None:
        for column_in_row_id in range(len(row)):
            if column_in_row_id == column_id:
                row[column_in_row_id] = self.__apply_masks(row[column_in_row_id], masks)

    def __apply_mask_to_columns_diff(
        self, columns_diff: List[ColumnDiff], column_id: int, masks: List[Mask]
    ) -> None:
        for column_diff in columns_diff:
            if column_diff.column_number != column_id:
                continue

            if column_diff.old_value:
                column_diff.old_value = self.__apply_masks(column_diff.old_value, masks)
            if column_diff.new_value:
                column_diff.new_value = self.__apply_masks(column_diff.new_value, masks)

    @staticmethod
    def __apply_masks(value: str, masks: List[Mask]) -> str:
        for mask in masks:
            value = mask.apply(value)
        return value
